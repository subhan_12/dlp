<?php 
session_start();

if(!isset($_SESSION["user_id"]))
{
header("location:index.php");
}

include('config.php'); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Learners</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap-responsive.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">

    <script src="lib/jquery-1.8.1.min.js" type="text/javascript"></script>


		<script type="text/javascript" charset="utf-8" src="datatables/media/js/jquery.js"></script>
		<script type="text/javascript" charset="utf-8" src="datatables/media/js/jquery.dataTables.js"></script>
		<script type="text/javascript" charset="utf-8" src="datatables/media/js/ZeroClipboard.js"></script>
		<script type="text/javascript" charset="utf-8" src="datatables/media/js/TableTools.js"></script>
		<script type="text/javascript" charset="utf-8" src="http://datatables.github.com/Plugins/integration/bootstrap/dataTables.bootstrap.js"></script>
		<script type="text/javascript" charset="utf-8">
			$(document).ready( function () {
				$('#example').dataTable( {
					"sDom": "<'row-fluid'<'span6'T><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
					"oTableTools": {
					 "sSwfPath": "/apricot/panel/datatables/swf/copy_csv_xls_pdf.swf"
       }
				} );
			} );
		</script>


	
    <!-- Demo page code -->
    
    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .brand { font-family: georgia, serif; }
        .brand .first {
            color: #ccc;
            font-style: italic;
        }
        .brand .second {
            color: #fff;
            font-weight: bold;
        }
		.DTTT_button_print {
		visibility:hidden;
		}
		.DTTT_button_copy {
		display:none;
		}
		.DTTT_button_csv {
		border-left:2px solid #CCCCCC;
		}
		.dataTables_filter {
		float:right;
		}
		.pagination {
		float:right;
		}
    </style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
  <!--[if IE 7 ]> <body class="ie ie7"> <![endif]-->
  <!--[if IE 8 ]> <body class="ie ie8"> <![endif]-->
  <!--[if IE 9 ]> <body class="ie ie9"> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> 
  <body> 
  <!--<![endif]-->
    
    <div class="navbar">
        <div class="navbar-inner">
            <div class="container-fluid">
                <ul class="nav pull-right">
                    
                    <li id="fat-menu" class="dropdown">
                        <a href="#" id="drop3" role="button" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-user"></i> Admin
                            <i class="icon-caret-down"></i>
                        </a>

                        <ul class="dropdown-menu">
                            <li><a tabindex="-1" href="scripts/logout.php">Logout</a></li>
                        </ul>
                    </li>
                    
                </ul>
                <a class="brand" href="index.html"><span class="second">Apricot</span></a>
            </div>
        </div>
    </div>
    

    <div class="container-fluid">
        
        <div class="row-fluid">
            <div class="span3">
                <div class="sidebar-nav">
                  <div class="nav-header" data-toggle="collapse" data-target="#dashboard-menu"><i class="icon-dashboard"></i>Dashboard</div>
                  <ul id="dashboard-menu" class="nav nav-list collapse in">
                        <li><a href="dashboard.php">Home</a></li>
                        <li><a href="instructor.php">Instructor</a></li>
                        <li class="active"><a href="learner.php">Learner</a></li>
                        <li ><a href="settings.php">Settings</a></li>
                        
                    </ul>

            </div>
        </div>
        <div class="span9">
            <h1 class="page-title">Learners</h1>
<!-- <input type="text" class="span2" id="id_search" placeholder="search">-->
                   
<!--<button type="submit" class="btn btn-primary pull-right"><a href="html2pdf/examples/instructor_report.php" style="color:#fff;">Report</a></button>-->
<!--
<div class="btn-toolbar">
    <button class="btn btn-primary"><i class="icon-plus"></i> New User</button>
    <button class="btn">Import</button>
    <button class="btn">Export</button>
  <div class="btn-group">
  </div>
</div> -->
<div class="well">

    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#deletesuccess').delay(3000).fadeOut();
      });
    </script>

<?php if($_GET[d]) { ?>
<div class="alert alert-success" id="deletesuccess">
Your Data deleted.
</div><? } ?>

<div style="overflow-x:scroll;">
  <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
	<thead>
		<tr>
		  <th style="font-size:13px; white-space:nowrap"><b>ADI no.</b></th>
          <th style="font-size:13px; white-space:nowrap"><b>First Name</b></th>
          <th style="font-size:13px; white-space:nowrap"><b>Last Name</b></th>
		  <th style="font-size:13px;"><b>Email</b></th>
		  <th style="font-size:13px; white-space:nowrap"><b>House no./name</b></th>
		  <th style="font-size:13px; white-space:nowrap"><b>Street 1</b></th>
		  <th style="font-size:13px; white-space:nowrap"><b>Street 2</b></th>
		  <th style="font-size:13px;"><b>Town</b></th>
		  <th style="font-size:13px; white-space:nowrap"><b>Post Code</b></th>
		  <th style="font-size:13px;"><b>Mobile</b></th>
		  <th style="font-size:13px; white-space:nowrap"><b>Learner Type</b></th>
          <th style="font-size:13px; white-space:nowrap"><b>License Number</b></th>
		</tr>
	</thead>

	<tbody>
<?php

$select = "SELECT * FROM learner ORDER BY ID ASC";
$result = mysql_query($select);

while($row = mysql_fetch_array($result)){
 $id =  $row['user_id'];
$street = $row['street'];

list($s1,$s2) = explode('*', $street);


$selects = "SELECT * FROM user WHERE id = $id";
$results = mysql_query($selects);

while($rows = mysql_fetch_array($results)){
$email =  $rows['email'];
}   
?>
        <tr class="odd_gradeA">
		  <td style="font-size:13px; white-space:nowrap"><?php echo $row['adi_number']; ?></td>
          <td style="font-size:13px; white-space:nowrap"><?php echo $row['first_name']; ?></td>
          <td style="font-size:13px; white-space:nowrap"><?php echo $row['last_name']; ?></td>
		  <td style="font-size:13px;"><?php echo $email; ?></td>
		  <td style="font-size:13px; white-space:nowrap"><?php echo $row['home_name_number']; ?></td>
		  <td style="font-size:13px; white-space:nowrap"><?php echo $s1; ?></td>
	      <td style="font-size:13px; white-space:nowrap"><?php echo $s2; ?></td>
	      <td style="font-size:13px; white-space:nowrap"><?php echo $row['town']; ?></td>
	      <td style="font-size:13px; white-space:nowrap"><?php echo $row['postal_code']; ?></td>
	      <td style="font-size:13px; white-space:nowrap"><?php echo $row['mobile']; ?></td>
	      <td style="font-size:13px; white-space:nowrap"><?php echo $row['learner_type']; ?></td>
	      <td style="font-size:13px; white-space:nowrap"><?php echo $row['license_number']; ?></td>
	    </tr>
<? } ?>    
		
	</tbody>
</table>

</div>
</div>

        </div>
    </div>
    

   <!-- 
    <footer>
        <hr>
        <!-- Purchase a site license to remove this link from the footer: http://www.portnine.com/bootstrap-themes
        <p class="pull-right">A <a href="http://www.portnine.com/bootstrap-themes" target="_blank">Free Bootstrap Theme</a> by <a href="http://www.portnine.com" target="_blank">Portnine</a></p>
        
        
        <p>&copy; 2012 <a href="http://www.portnine.com">Portnine</a></p>
    </footer>
    -->

    

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="lib/bootstrap/js/bootstrap.js"></script>
	<script src="resource/jquery.quicksearch.js"></script>
    <script type="text/javascript">
$(function(){
$('input#id_search').quicksearch('.table tbody tr', {
  'delay' : 300,
 });
});
</script>
  </body>
</html>


