-- MySQL dump 10.13  Distrib 5.1.70, for unknown-linux-gnu (x86_64)
--
-- Host: www.swenggcosoftware.net    Database: softnet_apricot
-- ------------------------------------------------------
-- Server version	5.1.70-cll

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'admin','admin','subhan_zaidi@yahoo.com');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `booking`
--

DROP TABLE IF EXISTS `booking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `instructor_user_id` int(20) DEFAULT NULL,
  `learner_user_id` int(20) DEFAULT NULL,
  `pick_up_location` varchar(255) DEFAULT NULL,
  `drop_off_location` varchar(255) DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `placeholder` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=278 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booking`
--

LOCK TABLES `booking` WRITE;
/*!40000 ALTER TABLE `booking` DISABLE KEYS */;
INSERT INTO `booking` VALUES (232,123,124,'Home','Home','16:13:00','05 Minutes','2013-07-11','Enter Here',1),(233,123,124,'Home','Home','16:13:00','05 Minutes','2013-07-13','Enter Here',1),(234,123,126,'Home','Home','12:00:00','20 Minutes','2013-07-17','Enter Here',1),(235,123,126,'Home','Home','12:00:00','20 Minutes','2013-07-17','Enter Here',1),(236,123,126,'Home','Home','12:00:00','20 Minutes','2013-07-17','Enter Here',1),(243,123,124,'College','College','16:40:00','05 Minutes','2013-07-10','Enter Here',1),(244,123,124,'Home','Home','16:41:00','05 Minutes','2013-07-12','Enter Here',1),(249,136,137,'Home','College','02:36:00','05 Minutes','2013-07-13','Text \n',1),(250,123,124,'Home','Home','13:39:00','05 Minutes','2013-07-23','Enter Here',1),(255,123,126,'Home','Home','12:00:00','20 Minutes','2013-07-17','Enter Here',1),(256,123,124,'Home','Home','12:47:00','10 Minutes ','2013-07-16','Enter Here',1),(259,132,133,'Home','Home','10:03:00','05 Minutes','2013-07-16','Enter Here',1),(260,132,133,'Home','Home','10:04:00','05 Minutes','2013-07-17','Enter Here',1),(261,156,157,'Home','Home','17:30:00','30 Minutes','2013-07-16','Enter Here',1),(262,156,158,'College','Work','15:40:00','60 Minutes','2013-07-16','This is a testing',1),(263,156,157,'Home','Home','12:00:00','30 Minutes','2013-07-17','Enter Here',1),(264,156,158,'Home','Home','12:00:00','60 Minutes','2013-07-17','Enter Here',1),(266,123,126,'Home','Home','12:00:00','20 Minutes','2013-07-17','Enter Here',1),(267,134,159,'College','College','12:00:00','10 Minutes ','2013-07-16','Enter Here',1),(268,134,159,'Home','Home','12:30:00','75 Minutes','2013-07-17','Enter Here',1),(269,156,160,'College','Home','14:24:00','30 Minutes','2013-07-15','Enter Here',1),(272,153,155,'College','College','13:10:00','05 Minutes','2013-07-15','Enter Here',1),(273,153,161,'Home','Work','13:42:00','05 Minutes','2013-07-16','Enter Here',1),(274,153,161,'Home','Work','13:42:00','05 Minutes','2013-07-16','Enter Here',1),(275,153,161,'Home','Work','13:42:00','05 Minutes','2013-07-16','Enter Here',1),(276,153,161,'Home','Work','13:42:00','05 Minutes','2013-07-16','Enter Here',1),(277,153,161,'Home','Work','13:42:00','05 Minutes','2013-07-16','Enter Here',1);
/*!40000 ALTER TABLE `booking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_token`
--

DROP TABLE IF EXISTS `device_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_token` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(20) DEFAULT NULL,
  `device_token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=161 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_token`
--

LOCK TABLES `device_token` WRITE;
/*!40000 ALTER TABLE `device_token` DISABLE KEYS */;
INSERT INTO `device_token` VALUES (6,'7','8D65EAFC-376C-40C0-96EF-95A45BCFD1ED'),(7,'8',''),(8,'9',''),(9,'10',''),(10,'11',''),(11,'12',''),(12,'13',''),(13,'14',''),(14,'15',''),(15,'16',''),(16,'17',''),(17,'18',''),(18,'19',''),(19,'20',''),(20,'21',''),(21,'22',''),(22,'23',''),(23,'24',''),(24,'25',''),(25,'26',''),(26,'27',''),(27,'28',''),(28,'29',''),(29,'30',''),(30,'31','F1603CB5-AEDE-4C88-B72E-99FFA41267E7'),(31,'32',''),(32,'33','F1603CB5-AEDE-4C88-B72E-99FFA41267E7'),(33,'34',''),(34,'35',''),(35,'36','F6AA8585-D1A0-417A-964E-7BF620A5572D'),(36,'37',''),(37,'38',''),(38,'39',''),(39,'40',''),(40,'41',''),(41,'42',''),(42,'43',''),(43,'44','8089DF64-5C0E-4D9C-B7B9-DF0AB5CC56BE'),(44,'45','95F41599-B2B2-4E87-BE6C-20042A503393'),(45,'46',''),(46,'47','CFE0D833-3D34-4F0E-B404-38B8277C8F23'),(47,'48',''),(48,'49',''),(49,'50',''),(50,'51',''),(51,'52',''),(52,'53',''),(53,'54','F6AA8585-D1A0-417A-964E-7BF620A5572D'),(54,'55',''),(55,'56',''),(56,'57',''),(57,'58',''),(58,'59',''),(59,'60',''),(60,'61',''),(61,'62',''),(62,'63',''),(63,'64',''),(64,'65','F1603CB5-AEDE-4C88-B72E-99FFA41267E7'),(65,'66','791C9AD0-1FFB-4EA1-8620-427836B919FA'),(66,'67','F1603CB5-AEDE-4C88-B72E-99FFA41267E7'),(67,'68',''),(68,'69',''),(69,'70',''),(70,'71',''),(71,'72',''),(72,'73',''),(73,'74',''),(74,'75',''),(75,'76',''),(76,'77',''),(77,'78',''),(78,'79','F1603CB5-AEDE-4C88-B72E-99FFA41267E7'),(79,'80','F1603CB5-AEDE-4C88-B72E-99FFA41267E7'),(80,'81','F1603CB5-AEDE-4C88-B72E-99FFA41267E7'),(81,'82','F1603CB5-AEDE-4C88-B72E-99FFA41267E7'),(82,'83','F1603CB5-AEDE-4C88-B72E-99FFA41267E7'),(83,'84',''),(84,'85',''),(85,'86',''),(86,'87',''),(87,'88',''),(88,'89',''),(89,'90',''),(90,'91',''),(91,'92',''),(92,'93',''),(93,'94',''),(94,'95',''),(95,'96',''),(96,'97',''),(97,'98',''),(98,'99',''),(99,'100',''),(100,'101',''),(101,'102',''),(102,'103',''),(103,'104',''),(104,'105',''),(105,'106',''),(106,'107',''),(107,'108',''),(108,'109',''),(109,'110',''),(110,'111',''),(111,'112',''),(112,'113',''),(113,'114',''),(114,'115',''),(115,'116',''),(116,'117',''),(117,'118','9697AC3D-A9A7-4D42-BD0A-19ED5C6F4165'),(118,'119',''),(119,'120',''),(120,'121','CFE0D833-3D34-4F0E-B404-38B8277C8F23'),(121,'122','E6A6F96A-F007-439A-A9DC-604124DB16BB'),(122,'123','21A91B1A-AA1E-4573-A7BC-2CC81AE9CDCB'),(123,'124','21A91B1A-AA1E-4573-A7BC-2CC81AE9CDCB'),(124,'125','21A91B1A-AA1E-4573-A7BC-2CC81AE9CDCB'),(125,'126','21A91B1A-AA1E-4573-A7BC-2CC81AE9CDCB'),(126,'127',''),(127,'128',''),(128,'129','21A91B1A-AA1E-4573-A7BC-2CC81AE9CDCB'),(129,'130','13CB0AAD-2825-420F-9463-871B2BA16884'),(130,'131','21A91B1A-AA1E-4573-A7BC-2CC81AE9CDCB'),(131,'132','21A91B1A-AA1E-4573-A7BC-2CC81AE9CDCB'),(132,'133','21A91B1A-AA1E-4573-A7BC-2CC81AE9CDCB'),(133,'134','21A91B1A-AA1E-4573-A7BC-2CC81AE9CDCB'),(134,'135','51C76901-DD5A-47EF-AF28-1C4098B54CA1'),(135,'136','51C76901-DD5A-47EF-AF28-1C4098B54CA1'),(136,'137','51C76901-DD5A-47EF-AF28-1C4098B54CA1'),(137,'138','51C76901-DD5A-47EF-AF28-1C4098B54CA1'),(138,'139','51C76901-DD5A-47EF-AF28-1C4098B54CA1'),(139,'140',''),(140,'141',''),(141,'142','55DC75FF-5069-4B70-9D83-85351DB520D9'),(142,'143',''),(143,'144',''),(144,'145',''),(145,'146','E6A6F96A-F007-439A-A9DC-604124DB16BB'),(146,'147',''),(147,'148','21A91B1A-AA1E-4573-A7BC-2CC81AE9CDCB'),(148,'149',''),(149,'150',''),(150,'151',''),(151,'152',''),(152,'153','DBC7E27F-5D5E-4F81-9665-6A06429038B2'),(153,'154',''),(154,'155',''),(155,'156','23F285D1-693C-4649-AA27-8908DA0F4354'),(156,'157',''),(157,'158',''),(158,'159',''),(159,'160',''),(160,'161','');
/*!40000 ALTER TABLE `device_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instructor`
--

DROP TABLE IF EXISTS `instructor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instructor` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(20) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `home_name_number` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `town` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `adi_number` varchar(255) DEFAULT NULL,
  `school_name` varchar(255) DEFAULT NULL,
  `school_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instructor`
--

LOCK TABLES `instructor` WRITE;
/*!40000 ALTER TABLE `instructor` DISABLE KEYS */;
INSERT INTO `instructor` VALUES (68,118,'Mr.','aaa','aaa','10','Abc street','Sialkot','51310','8888888','000000','abc school','abc'),(69,119,' Mr','learner1','learner2','1','street line 1*street line 2','town','df12 2qa','07079 363688','123456','adi 123',' Single Instructor'),(70,123,' Mr.','Aamer','Khalid','House No 1401','Kotli Loharan West','Sialkot','51310','07073 033333','111111','Aamer School',''),(71,125,' Mr','Muhammad Bilal','Saeed','271 Javaid House','Mir Hassan Road*Model Town','Sialkot','51310','07961 100000','000000','Swenggco Software.com',' Single Instructor'),(72,132,' Mr','Instructor','trainer','155/C gulberg','street line 1  street line 2','Lahore','112334445','07887 645132','455455','school name',''),(73,134,' Mr','newe','instructor','house number123','street street3','gojra','125263848','07512 454885','524348','gshhshshwgwgs',' Single Instructor'),(74,136,' Mr','fg','ff','fg','f*f','f','f','0755','999999','hh',' Single Instructor'),(75,138,' Ms','gh','hh','q','1 t','g','g','07123 456789','777777','ggg',''),(76,142,' Mr','Khurram','Iqbal','Main Street 1','Near Cyberhighway Street 3 is also compulsory','Tuttlingen','51310','01753 361951','951235','Khurram Driving School',''),(77,153,' Mr','Shoaib','Mehmood','House Number','Street 1*Street 2','Town','Post ','07711 670000','746242','Vocational ',' Single Instructor'),(78,156,' Mr','Muhammad Bilal','Saeed','271 Javaid House','Mir Hassan Road*Saroosh Park','Sialkot','51310','07961 100000','245255','Swenggco Software',' Multiple Instructor ');
/*!40000 ALTER TABLE `instructor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `learner`
--

DROP TABLE IF EXISTS `learner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `learner` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(20) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `home_name_number` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `town` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `learner_type` varchar(255) DEFAULT NULL,
  `license_number` varchar(255) DEFAULT NULL,
  `adi_number` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `learner`
--

LOCK TABLES `learner` WRITE;
/*!40000 ALTER TABLE `learner` DISABLE KEYS */;
INSERT INTO `learner` VALUES (48,120,'Mr.','Husnain','Afzal','10','Abc street','Sialkot','51310','0000','0000','0000','123456'),(49,121,' Mr','amir','khalid','abc','xyz*xyz','vhf','36788','07258 885554','Concession','FHTF2567dGGgG','43545'),(50,122,' Mr','aamer','khalid','ety','ety*ety','fgg','fgg','07235 888888','Employed ','cvffg','345345'),(51,124,' Mr','Naeem','Shakir','271-272 Mir Hassan Road','Model Town Sialkot','Punjab','51310','07333 333333','Student','ghdfgg','111111'),(52,126,' Miss','nasreen ','Mochan','house number 235 BC weref.','street number 1*street line 2','Sialkot','51310','07213 213213','Employed ','sdfdsfsafsadfasdfs','111111'),(53,129,' Mr','Muhammad ','Amjad','sdasdasd','sdasd*sadas','das','sadasd','07232 423432','Concession','sadasd23423wrewrwe','111111'),(54,130,' Mr','qaiser','ali','sff','sff*dfge','dry','sgfsrr','07122 354555','Concession','steryugDftfdx4567','0'),(55,131,' Mr','Main','ahmad','1401','kjk*kljkllkj','837489','23432','07258 963222','Concession','eirweopriwepoiropi','111111'),(56,133,' Mr','Learner','trainee','455/E.B ','street 1 street 2','Burewala','61100','07273 787778','','','455455'),(57,135,' Mr','ggg','vgg','d','d*d','d','d','07123 456789','Employed ','11233','123456'),(58,137,' Mr','fv','fg','3','d*d','d','d','07123 665488','Employed ','123','999999'),(59,139,' Mr','g','g','yhh','ggh*ggg','gvv','gv','07123 456999','Employed ','23577Â£!?)Â£&!(;,?Â£&','777777'),(60,140,' Mr','g','g','4','frf*f','frff','f','07123 456789','Employed ','13344;555566666666','777777'),(61,141,' Mr','abcd','wxyz','test','1*2','Sialkot','4444444','07000 000000','Employed ','','0'),(62,143,' Mr','Aamer','Khalid','222','1*3','town','51144','07000 000000','Student','','0'),(63,144,' Miss','f','f','f','ff*f','f','f','07000 000000','Concession','','0'),(64,145,' Mr','sabir','javed','271-272 mir Hassan road','street  one*street two','sialkot','51310','07494 958261','Employed ','sndjfjdjwjdjskeksk','111111'),(65,146,' Mr','Akhter','Ali','231-moon market Lahore','Street 1*Street 2','Lahore','52133','07243 554543','Employed ','gghsfghrtjtyjtyejt','111111'),(66,147,' Mr','abc','assa','43423','dfds*eefsd','dfsdf','324234','07435 345454','Concession','4','455455'),(67,148,' Mr','werewrerer','rwer','asdasd','sadasd*dasd','asdas','sadsad','07345 345435','Concession','aedfdsf','111111'),(68,149,' Mr','gg','gg','sdasdas','dsa*d','as','d','07543 534544','Concession','dsfsdfdsf','111111'),(69,150,' Mr','wqerwqer','wqew','dsljfk','dsfds*fdsf','sdf','dsfds','07453 453453','Concession','fdsfsdf','111111'),(70,151,' Mr','b','b','b','brb*brb','b','v','07000 000000','Student','','0'),(71,152,' Mr','fg','fg','ddd','ddff*ffft','dffff','dffff','07512 488965','Employed ','gghhhh','111111'),(72,154,' Mr','Salman','Khalid','house','street*street 2222','Sialkot','51310','07333 333333','Employed ','','746242'),(73,155,' Mr','Tanveer','Aaa','address','street*saroosh','sialkot','postal','07999 999999','Student','000000','746242'),(74,157,' Mr','Aezaz','Haq','House','Dreeet*shhh','Sialkot','440000','07888 888888','Concession','','245255'),(75,158,' Mr','Aamer','Khalid','swenggco house','saroosh park*perfect','Sialkot','51310','07666 666666','Employed ','','245255'),(76,159,' Mr','Aamer','Khalid','1401','Kotli Loharan West*Sialkot','Sialkot','51310','07222 222222','Concession','GHJG234324GHJ32HG4','524348'),(77,160,' Miss','Miss','Dlp','1','1*q','q','q','07000 000000','Student','','245255'),(78,161,' Mr','v','g','hazy','qw*as','yxcc','2366','07035 658828','Student','','746242');
/*!40000 ALTER TABLE `learner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(10) NOT NULL,
  `status_act` int(1) NOT NULL DEFAULT '1',
  `device_token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=162 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (118,'billu2003@gmail.com','123456','I',1,NULL),(119,'test@test.com','password123','I',1,NULL),(120,'g@gh.com','password123','L',1,NULL),(121,'a@a.com','aaaaaa','L',1,NULL),(122,'aa@aa.com','aaaaaa','L',1,NULL),(123,'am@am.com','ssssss','I',1,NULL),(124,'naeem.shakir@live.com','Naeem123','L',1,NULL),(125,'test@dlp.com','123456','I',1,NULL),(126,'nasreen@mochan.com','aaaaaa','L',1,NULL),(127,'q@q.com','qqqqqq','L',1,NULL),(128,'qa@qa.com','qqqqqq','L',1,NULL),(129,'ad@ad.com','aaaaaa','L',1,NULL),(130,'qa@qq.com','qqqqqq','L',1,NULL),(131,'ah@ah.com','hhhhhh','L',1,NULL),(132,'inst@inst.com','aaaaaa','I',1,NULL),(133,'l@l.com','aaaaaa','L',1,NULL),(134,'gl@gl.com','aaaaaa','I',1,NULL),(135,'g@g.com','pass123','L',1,NULL),(136,'d@d.com','pass123','I',1,NULL),(137,'z@z.com','pass123','L',1,NULL),(138,'ins@b.com','pass123','I',1,NULL),(139,'a@c.com','pass123','L',1,NULL),(140,'b@b.com','pass123','L',1,NULL),(141,'test@test.uk','123456','L',1,NULL),(142,'khurram.iqbal@yahoo.com','abc123','I',1,NULL),(143,'a@ak.co','123456','L',1,NULL),(144,'f@f.com','123456','L',1,NULL),(145,'ja@ja.com','aaaaaa','L',1,NULL),(146,'ak@ak.com','aaaaaa','L',1,NULL),(147,'asdf@a.com','aaaaaa','L',1,NULL),(148,'ff@ff.com','ffffff','L',1,NULL),(149,'ggg@g.com','gggggg','L',1,NULL),(150,'qw@qw.com','qqqqqq','L',1,NULL),(151,'b@b.bb','123456','L',1,NULL),(152,'fg@fg.com','aaaaaa','L',1,NULL),(153,'birdeyeview.allright@gmail.com','123456','I',1,NULL),(154,'salman.swenggco@gmail.com','123456','L',1,NULL),(155,'tanveer.swenggco@gmail.com','123456','L',1,NULL),(156,'saeed.muhammadbilal@gmail.com','123456','I',1,NULL),(157,'aezazforu@gmail.com','123456','L',1,NULL),(158,'aamerkhalid1987@gmail.com','123456','L',1,NULL),(159,'aamirkhalid1987@gmail.com','aaaaaa','L',1,NULL),(160,'miss@dlp.com','123456','L',1,NULL),(161,'r@c.uk','qwertzuiop','L',1,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-07-16  4:38:17
