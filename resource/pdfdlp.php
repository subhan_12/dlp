<?php
require('fpdf17/fpdf.php'); 

class PDF extends FPDF {

        function BuildTable($header,$data) {

        //Colors, line width and bold font

        $this->SetFillColor(140,136,136);

        $this->SetTextColor(255);

        $this->SetDrawColor(204,198,198);

        $this->SetLineWidth(.3);

        $this->SetFont('','B');

        //Header

        // make an array for the column widths

        $w=array(20,20,20,20,20,20,20,20,20,20,20);

        // send the headers to the PDF document

        for($i=0;$i<count($header);$i++)

        $this->Cell($w[$i],7,$header[$i],1,0,'C',1);

        $this->Ln();

        //Color and font restoration

        $this->SetFillColor(175);

        $this->SetTextColor(0);

        $this->SetFont('');



        //now spool out the data from the $data array

        $fill=true; // used to alternate row color backgrounds

        foreach($data as $row)

        {

        $this->Cell($w[0],6,$row[0],'LR',0,'C',$fill);
 		$this->Cell($w[1],6,$row[1],'LR',0,'C',$fill);
		 $this->Cell($w[2],6,$row[2],'LR',0,'C',$fill);
		 $this->Cell($w[3],6,$row[3],'LR',0,'C',$fill);
		 $this->Cell($w[4],6,$row[4],'LR',0,'C',$fill);
		 $this->Cell($w[5],6,$row[5],'LR',0,'C',$fill);
		 $this->Cell($w[6],6,$row[6],'LR',0,'C',$fill);
		 $this->Cell($w[7],6,$row[7],'LR',0,'C',$fill);
		 $this->Cell($w[8],6,$row[8],'LR',0,'C',$fill);
		 $this->Cell($w[9],6,$row[9],'LR',0,'C',$fill);
		 $this->Cell($w[10],6,$row[10],'LR',0,'C',$fill);
	
        // set colors to show a URL style link

        $this->SetTextColor(0,0,0);

        $this->Ln();

        // flips from true to false and vise versa

        $fill =! $fill;

        }

        $this->Cell(array_sum($w),0,'','T');

        }

}

include ("../config.php");
$sql = "select * from learner";
$result = mysql_query($sql, $con) or die( "Could not execute sql: $sql");

// build the data array from the database records.

While($rows = mysql_fetch_array($result)) {

        $data[] = array($rows['first_name'], $rows['first_name'],$rows['last_name'],$rows['home_name_number'],$rows['street'],$rows['town'],$rows['postal_code'],$rows['mobile'],$rows['learner_type'],$rows['license_number'],$rows['adi_number']);

}
// start and build the PDF document

$pdf = new PDF();

//Column titles

$header=array('Title','First Name','Last Name', 'Home', 'Street', 'Town', 'Postal code', 'Mobile', 'Learner type', 'ADI no.');

$pdf->SetFont('Arial','',14);

$pdf->AddPage();

// call the table creation method

$pdf->BuildTable($header,$data);

$pdf->Output();

?>