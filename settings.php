<?php 

session_start();
if(!isset($_SESSION["user_id"]))
{
header("location:index.php");
}

include('config.php'); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Settings</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap-responsive.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">

    <script src="lib/jquery-1.8.1.min.js" type="text/javascript"></script>

    <!-- Demo page code -->
    
    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .brand { font-family: georgia, serif; }
        .brand .first {
            color: #ccc;
            font-style: italic;
        }
        .brand .second {
            color: #fff;
            font-weight: bold;
        }
    </style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
  <!--[if IE 7 ]> <body class="ie ie7"> <![endif]-->
  <!--[if IE 8 ]> <body class="ie ie8"> <![endif]-->
  <!--[if IE 9 ]> <body class="ie ie9"> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> 
  <body> 
  <!--<![endif]-->
    
    <div class="navbar">
        <div class="navbar-inner">
            <div class="container-fluid">
                <ul class="nav pull-right">
                    
                    <li id="fat-menu" class="dropdown">
                        <a href="#" id="drop3" role="button" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-user"></i> Admin
                            <i class="icon-caret-down"></i>
                        </a>

                        <ul class="dropdown-menu">
                            <li><a tabindex="-1" href="scripts/logout.php">Logout</a></li>
                        </ul>
                    </li>
                    
                </ul>
                <a class="brand" href="index.html"><span class="second">Apricot</span></a>
            </div>
        </div>
    </div>
    

    <div class="container-fluid">
        
        <div class="row-fluid">
            <div class="span3">
                <div class="sidebar-nav">
                  <div class="nav-header" data-toggle="collapse" data-target="#dashboard-menu"><i class="icon-dashboard"></i>Dashboard</div>
                  <ul id="dashboard-menu" class="nav nav-list collapse in">
                        <li><a href="dashboard.php">Home</a></li>
                        <li ><a href="instructor.php">Instructor</a></li>
                        <li ><a href="learner.php">Learner</a></li>
                        <li class="active"><a href="settings.php">Settings</a></li>
                        
                    </ul>

            </div>
        </div>
        <div class="span9">
            <h1 class="page-title">Settings</h1>
<div class="well">
<ul class="nav nav-tabs">
  <li class="active">
    <a href="#">Settings</a>
  </li>
  <li><a href="backup.php">Backup</a></li>
</ul>

<script>

function abc(){
var old_p = document.getElementById('old_p').value;
var new_p = document.getElementById('new_p').value;
var re_p = document.getElementById('re_p').value;


if(old_p.trim() == "" && new_p.trim() == "" && re_p.trim()=="") { 
document.forms["signin"]["old_p"].style.border = "1px solid red";
document.forms["signin"]["new_p"].style.border = "1px solid red";
document.forms["signin"]["re_p"].style.border = "1px solid red";
return false;
}
if(new_p.trim() == "" && re_p.trim()=="") { 
document.forms["signin"]["new_p"].style.border = "1px solid red";
document.forms["signin"]["re_p"].style.border = "1px solid red";
return false;
}

if(old_p.trim() == "" && re_p.trim()=="") { 
document.forms["signin"]["old_p"].style.border = "1px solid red";
document.forms["signin"]["re_p"].style.border = "1px solid red";
return false;
}

if(new_p.trim() == "" && old_p.trim()=="") { 
document.forms["signin"]["new_p"].style.border = "1px solid red";
document.forms["signin"]["old_p"].style.border = "1px solid red";
return false;
}

 if(re_p.trim() == "") { 
document.forms["signin"]["re_p"].style.border = "1px solid red";
return false;
}

if(new_p.trim() == "") { 
document.forms["signin"]["new_p"].style.border = "1px solid red";
return false;
}

 if(old_p.trim() == "") { 
document.forms["signin"]["old_p"].style.border = "1px solid red";
return false;
}
if(new_p != re_p) { 
document.forms["signin"]["new_p"].style.border = "1px solid red";
document.forms["signin"]["re_p"].style.border = "1px solid red";
return false;
}
return true;
}

function olds(){
document.forms["signin"]["old_p"].style.border = "1px solid #C0C0C0";
return false;
}

function olds_blur(){
var old_p = document.getElementById('old_p').value;
if(old_p!=""){
document.forms["signin"]["old_p"].style.border = "1px solid green";
return false;
}
}

function news(){
document.forms["signin"]["new_p"].style.border = "1px solid #C0C0C0";
return false;
}

function news_blur(){
var new_p = document.getElementById('new_p').value;
if(new_p!=""){
document.forms["signin"]["new_p"].style.border = "1px solid green";
return false;
}
}


function res(){
document.forms["signin"]["re_p"].style.border = "1px solid #C0C0C0";
return false;
}

function res_blur(){
var re_p = document.getElementById('re_p').value;
if(re_p!=""){
document.forms["signin"]["re_p"].style.border = "1px solid green";
return false;
}
}
</script>
<div class="dialog span4">

        <div class="block">
            <div class="block-heading">Change Password</div>
            <div class="block-body">

                <form name="signin" action="scripts/password_script.php" method="post" onsubmit="return abc();">
                    <label>Old Password</label>
                    <input type="password" class="span12" name="old" id="old_p" onfocus="return olds();" onblur="return olds_blur();" style="border:1px solid #C0C0C0;">
                    <label>New Password</label>
                    <input type="password" class="span12" name="new" id="new_p" onfocus="return news();" onblur="return news_blur();" style="border:1px solid #C0C0C0;">
					<label>Re-enter Password</label>
                    <input type="password" class="span12" name="re" id="re_p" onfocus="return res();" onblur="return res_blur();" style="border:1px solid #C0C0C0;">
					
					<?php if($_GET[msg]) { ?>
					<div class="alert alert-success">
					<strong>Success!</strong> Your Password has been changed.</div><? } ?>
					<? if($_GET[a]==a) { ?>
					<div class="alert alert-error">
  					<strong>Whoopss!!</strong> Your Old Password is not correct.</div><? } ?>				
					<div class="clearfix"></div>
					<button type="reset" class="btn btn-primary pull-left">Cancel</button>
                    <button type="submit" class="btn btn-primary pull-right">Save</button><br>
                    <!--<label class="remember-me"><input type="checkbox"> Remember me</label>-->

                </form>
            </div>
        </div>

    </div>
</div>

<div class="modal small hide fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Delete Confirmation</h3>
    </div>
    <div class="modal-body">
        <p class="error-text"><i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete the user?</p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
        <button class="btn btn-danger" data-dismiss="modal">Delete</button>
    </div>
</div>

        </div>
    </div>
    
 

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="lib/bootstrap/js/bootstrap.js"></script>
    

  </body>
</html>


">Free Bootstrap Theme</a> by <a href="http://www.portnine.com" target="_blank">Portnine</a></p>
        
        
        <p>&copy; 2012 <a href="http://www.portnine.com">Portnine</a></p>
    </footer>
    

    

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="lib/bootstrap/js/bootstrap.js"></script>
    

  </body>
</html>


