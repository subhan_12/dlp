<?php 
session_start();
if(!isset($_SESSION["user_id"]))
{
header("location:index.php");
}

include('config.php'); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Instructors</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap-responsive.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">

    <script src="lib/jquery-1.8.1.min.js" type="text/javascript"></script>
	
	
    <!-- Demo page code -->
    
    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .brand { font-family: georgia, serif; }
        .brand .first {
            color: #ccc;
            font-style: italic;
        }
        .brand .second {
            color: #fff;
            font-weight: bold;
        }
    </style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
  <!--[if IE 7 ]> <body class="ie ie7"> <![endif]-->
  <!--[if IE 8 ]> <body class="ie ie8"> <![endif]-->
  <!--[if IE 9 ]> <body class="ie ie9"> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> 
  <body> 
  <!--<![endif]-->
    
    
<div class="well">

    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#deletesuccess').delay(3000).fadeOut();
      });
    </script>

<?php if($_GET[d]) { ?>
<div class="alert alert-success" id="deletesuccess">
Your Data deleted.
</div><? } ?>

<div style="overflow-x:scroll;">
    <table class="table">
      <thead>
        <tr>
          <th>#</th>
		  <th>ADI no.</th>
          <th>Title</th>
          <th>First Name</th>
          <th>Last Name</th>
		  <th>Email</th>
		  <th>House no./name</th>
		  <th>Street 1 </th>
		  <th>Street 2</th>
		  <th>Town</th>
		  <th>Post Code</th>
		  <th>School Name</th>
		  <th>Mobile</th>
          <th>School Type</th>
          <th style="width: 26px;"></th>
        </tr>
      </thead>
      <tbody>


<?php

$select = "SELECT * FROM instructor ORDER BY ID ASC";
$result = mysql_query($select);

while($row = mysql_fetch_array($result)){
$id =  $row['user_id'];
$street = $row['street'];

list($s1,$s2) = explode('*', $street);


$selects = "SELECT * FROM user WHERE id = $id";
$results = mysql_query($selects);

while($rows = mysql_fetch_array($results)){
$email =  $rows['email'];
}
    
?>
        <tr>
          <td><?php echo $row['id']; ?></td>
		  <td><?php echo $row['adi_number']; ?></td>
          <td><?php echo $row['title']; ?></td>
          <td><?php echo $row['first_name']; ?></td>
          <td><?php echo $row['last_name']; ?></td>
		  <td><?php echo $email; ?></td>
          <td><?php echo $row['home_name_number']; ?></td>
          <td><?php echo $s1; ?></td>
          <td><?php echo $s2; ?></td>
          <td><?php echo $row['town']; ?></td>
          <td><?php echo $row['postal_code']; ?></td>
          <td><?php echo $row['school_name']; ?></td>
          <td><?php echo $row['mobile']; ?></td>
          <td><?php echo $row['school_type']; ?></td>			
         <!-- <td>

              <a href="edit-instructor.php?id=<?php echo $row['id'];?>"><i class="icon-pencil"></i></a>
              <a href="#myModal" data-value="<?php echo $row['id']; ?>" class="delete" role="button" data-toggle="modal"><i class="icon-remove"></i></a>
          </td>-->
        </tr>


<div class="modal small hide fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

<script type="text/javascript">
 $(function(){
  $(".delete").die().live('click', function(){

   var popId = $(this).data("value");

   $(".del").attr('href', "scripts/delete_instructor.php?id="+popId+"");
  });
 });
</script>

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Delete Confirmation</h3>
    </div>
    <div class="modal-body">
        <p class="error-text"><i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete the user?</p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
        <a class="btn btn-danger del" style="color:#fff;">Delete</a>
    </div>
</div>

<? } ?>        
      </tbody>
    </table>
</div>
</div>

        </div>
    </div>
    
    

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="lib/bootstrap/js/bootstrap.js"></script>
	<script src="resource/jquery.quicksearch.js"></script>
    <script type="text/javascript">
$(function(){
$('input#id_search').quicksearch('.table tbody tr', {
  'delay' : 300,
 });
});
</script>
  </body>
</html>


